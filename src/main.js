const express = require('express') ;
const apis = require('./api') ;
const utils = require('./util');
const app = express() ;
const path  = require('path');

app.use('/public', express.static(path.join(utils.rootPath, 'public')));
//app.get('/', apis.helloController) ;
//app.get('/hacker', apis.hackerController) ;
//app.get('/')z Z
app.get('/home', apis.homepageController);
app.get('/login', apis.loginController);
app.get('/register', apis.registerController);
app.get('/user', apis.userController);
app.get('/note', apis.notesController);
app.get('/user/:id', apis.idController);

const port = +process.env.POT || 6060;

app.listen(port, (error) => {
    if (error) {
        return console.error('run server got an error', error) ;
    }
    console.log(`Server listening ${port}`) ;
}) ;