const fs = require('fs') ;
const path = require('path') ;
const util = require('./util') ;

const homepageController = (req, res) => {
    const indexFile = path.join(util.rootPath, 'template', 'index.html') ;
    fs.readFile(indexFile, (err, content) => {
        if(err) {
            res.json({error: error }) ;

        } else {
            res.header('Content-Type', 'text/html');
            res.send(content);
        }
    })
};
const loginController = async (req, res) => {
    const indexFile = path.join(util.rootPath, 'template', 'login.html');
    try {
        const indexContent = await util.readFile(indexFile);
        res.header('Content-Type', 'text/html');
        res.send(indexContent);
    } catch (error) {
        res.json({ error: error }) ;
    }
};
const userController = (req, res) => {
  const indexFile = path.join(util.rootPath, 'data', 'users.json') ;
  fs.readFile(indexFile, (err, content) => {
      if(err) {
          res.json({error: error }) ;

      } else {
          res.header('Content-Type', 'text/json');
          res.send(content);
      }
  })
};
const registerController = (req, res) => {
    const registerHtml = path.join(util.rootPath, 'template', 'register.html') ;
    util.readFile(registerHtml)
    .then((content) => {
        res.header('Content-Type', 'text/html');
        res.send(content);
    })
    .catch((error) => {
        res.json({ error: error.message });
    })
};


const notesController = async (req, res) => {
  const indexFile = path.join(util.rootPath, 'data', 'notes.json');
  try {
      const indexContent = await util.readFile(indexFile);
      res.header('Content-Type', 'text/json');
      res.send(JSON.parse(indexContent));
  } catch (error) {
      res.json({ error: error }) ;
  }
};

const idController = async (req, res) => {
  const indexFile = path.join(util.rootPath, 'data', 'users.json');
  const indexFile2 = path.join(util.rootPath, 'data', 'notes.json');

  try {
    const indexContent = await util.readFile(indexFile); 
    
    const indexContent2 = await util.readFile(indexFile2);
    const users = JSON.parse(indexContent);
    const notes = JSON.parse(indexContent2);
    
    //res.header('content-Type', 'text/json');
    
    const userId = req.params.id;
    const kiemTra = users.findIndex(users => users.id === userId);

    if( kiemTra === -1 ) {
      res.send('sai id'); 
     } else {
       const xuat = notes.filter(note => note.createById === userId);
      res.send(xuat);
     };
    

  }catch (error) {
    res.json({ error: error}) ;
  }
};
module.exports = {
    homepageController,
    loginController,
    registerController,
    userController,
    notesController,
    idController,
};


